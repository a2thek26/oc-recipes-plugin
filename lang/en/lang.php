<?php

return [
    'general' => [
        'dropdown_option' => ['' => '-- Select One --']
    ],
    'recipes' => [
        'menu_label'             => 'Recipes',

        'name_label'             => 'Recipe Name',
        'slug_label'             => 'Slug',
        'description_label'      => 'Description',
        'prep_time_label'        => 'Prep Time',
        'cook_time_label'        => 'Cook Time',
        'instructions_label'     => 'Instructions',
        'author_label'           => 'Author',
        'difficulty_label'       => 'Difficulty',
        'images_label'           => 'Images',
        'published_label'        => 'Publish this recipe?',
        'published_list_label'   => 'Published',
        'tags_label'             => 'Tags',

        'tab_info_label'         => 'Recipe Information',
        'tab_instructions_label' => 'Instructions',
        'tab_ingredients_label'  => 'Ingredients',
        'tab_images_label'       => 'Images'
    ],
    'authors' => [
        'menu_label'           => 'Authors',

        'first_name_label'     => 'First Name',
        'last_name_label'      => 'Last Name',
        'title_label'          => 'Title',
        'bio_label'            => 'Bio',
        'published_label'      => 'Publish this author?',
        'published_list_label' => 'Published'
    ]
];
