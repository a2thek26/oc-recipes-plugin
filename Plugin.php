<?php namespace Devinci\Recipes;

use Backend;
use Devinci\Tags\Models\Tag;
use System\Classes\PluginBase;

/**
 * Recipes Plugin Information File
 */
class Plugin extends PluginBase
{

	public $require = ['Devinci.Tags'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Recipes',
            'description' => 'A beautiful recipe builder.',
            'author'      => 'Devinci',
            'icon'        => 'icon-fork'
        ];
    }

	/**
	 * @return array
	 */
    public function registerNavigation()
    {
        return [
            'recipes' => [
                'label'       => 'devinci.recipes::lang.recipes.menu_label',
                'url'         => Backend::url('devinci/recipes/recipes'),
                'icon'        => 'icon-calendar-o',
                'permissions' => ['devinci.recipes.*'],
                'order'       => 500,

                'sideMenu' => [
                    'recipes' => [
                        'label'       => 'devinci.recipes::lang.recipes.menu_label',
                        'url'         => Backend::url('devinci/recipes/recipes'),
                        'icon'        => 'icon-calendar-o',
                        'permissions' => ['devinci.recipes.manage_recipes'],
                        'order'       => 500,
                    ],
                    'authors' => [
                        'label'       => 'devinci.recipes::lang.authors.menu_label',
                        'url'         => Backend::url('devinci/recipes/authors'),
                        'icon'        => 'icon-pencil-square',
                        'permissions' => ['devinci.recipes.manage_authors'],
                        'order'       => 600,
                    ],
                ]
            ]
        ];
    }

	/**
	 * Add relationship to Tags
	 */
	public function boot()
	{
		Tag::extend(function($model){
			$model->morphedByMany['recipes'] = [
				'Devinci\Recipes\Models\Recipe',
				'table'      => 'devinci_tags_taggable',
				'name'       => 'taggable',
				'timestamps' => true
			];
		});
	}

}
