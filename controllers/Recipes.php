<?php namespace Devinci\Recipes\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Recipes Back-end Controller
 */
class Recipes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['devinci.recipes.*'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Devinci.Recipes', 'recipes', 'recipes');
    }
}
