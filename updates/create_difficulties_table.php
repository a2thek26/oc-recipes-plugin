<?php namespace Devinci\Recipes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateDifficultiesTable extends Migration
{

    public function up()
    {
        Schema::create('devinci_recipes_difficulties', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('devinci_recipes_difficulties');
    }

}
