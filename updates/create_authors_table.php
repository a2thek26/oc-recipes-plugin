<?php namespace Devinci\Recipes\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateAuthorsTable extends Migration
{

    public function up()
    {
        Schema::create('devinci_recipes_authors', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('title');
            $table->text('bio');
            $table->tinyInteger('is_published')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('devinci_recipes_authors');
    }

}
