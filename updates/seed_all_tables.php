<?php namespace Devinci\Recipes\Updates;

use Carbon\Carbon;
use Devinci\Recipes\Models\Author;
use Devinci\Recipes\Models\Difficulty;
use October\Rain\Database\Updates\Seeder;

class SeedAllTables extends Seeder
{

    public function run()
    {
        foreach(['Easy', 'Medium', 'Difficult'] as $difficulty) {
            Difficulty::create([
               'name'       => $difficulty,
               'code'       => lcfirst($difficulty),
               'created_at' => Carbon::now(),
               'updated_at' => null,
            ]);
        }

        // load fake author date for testing...
        $authors = [
            ['first_name' => 'Test', 'last_name' => 'Chef', 'title' => 'Chef'],
            ['first_name' => 'Test', 'last_name' => 'Author', 'title' => 'Sous Chef'],
        ];

        foreach($authors as $author) {
            Author::create([
                'first_name' => $author['first_name'],
                'last_name'  => $author['last_name'],
                'title'      => $author['title'],
                'bio'        => 'Lorem',
                'created_at' => Carbon::now(),
                'updated_at' => null,
            ]);
        }
    }

}
