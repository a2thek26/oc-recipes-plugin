<?php namespace Devinci\Recipes\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRecipesTable extends Migration
{

    public function up()
    {
        Schema::create('devinci_recipes_recipes', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('slug');
            $table->text('description')->nullable();
            $table->text('instructions')->nullable();
            $table->text('ingredients')->nullable();
            $table->string('prep_time')->nullable();
            $table->string('cook_time')->nullable();
            $table->unsignedInteger('author_id')->nullable();
            $table->unsignedInteger('difficulty_id')->nullable();
            $table->tinyInteger('is_published')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('devinci_recipes_recipes');
    }

}
