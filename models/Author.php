<?php namespace Devinci\Recipes\Models;

use Model;
use October\Rain\Database\Traits\SoftDeleting;

/**
 * Author Model
 */
class Author extends Model
{
    use  SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'devinci_recipes_authors';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['first_name', 'last_name', 'title', 'bio', 'is_published'];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'recipes' => 'Devinci\Recipes\Models\Recipe'
    ];

}
