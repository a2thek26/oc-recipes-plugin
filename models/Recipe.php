<?php namespace Devinci\Recipes\Models;

use Illuminate\Support\Facades\DB;
use Model;
use Devinci\Recipes\Models\Difficulty;
use October\Rain\Database\Traits\Validation;

/**
 * Recipe Model
 */
class Recipe extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'devinci_recipes_recipes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'slug', 'description', 'ingredients', 'instructions', 'prep_time', 'cook_time', 'author_id', 'difficulty_id', 'is_published'];

    /**
     * @var array Store as json
     */
    protected $jsonable = ['ingredients'];

	/**
	 * @var array Validation rules
	 */
    public $rules = [
		'name'          => 'required',
        'slug'          => 'required',
        'difficulty_id' => 'required'
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'difficulty' => 'Devinci\Recipes\Models\Difficulty',
        'author'     => 'Devinci\Recipes\Models\Author'
    ];

	/**
	 * @var array Polymorphic relations
	 */
	public $morphToMany = [
		'tags' => [
			'Devinci\Tags\Models\Tag',
			'name'  => 'taggable',
			'table' => 'devinci_tags_taggables',
			'key'   => 'taggable_id',
		]
	];

    /**
     * @var array Attachments
     */
    public $attachMany = [
        'featured_images' => ['System\Models\File', 'order' => 'sort_order'],
    ];

    /**
     * For difficulty dropdown
     *
     * @return mixed
     */
    public function getDifficultyOptions()
    {
        $options = Difficulty::lists('name', 'id');

        return trans('devinci.recipes::lang.general.dropdown_option') +  $options;
    }

    /**
     * For author dropdown
     *
     * @return mixed
     */
    public function getAuthorOptions()
    {
        $options = Author::select('id', DB::raw('CONCAT(first_name, " ", last_name) AS name'))
              ->orderBy('last_name')
              ->lists('name', 'id');

        return trans('devinci.recipes::lang.general.dropdown_option') + $options;
    }

}
