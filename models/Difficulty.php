<?php namespace Devinci\Recipes\Models;

use Model;

/**
 * Difficulty Model
 */
class Difficulty extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'devinci_recipes_difficulties';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'code'];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'recipes' => 'Devinci\Recipes\Models\Recipe'
    ];

}
